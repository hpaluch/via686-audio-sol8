
#
# Copyright (c) 1996,1997, by Sun Microsystems, Inc.
# All Rights Reserved
#
#ident	"@(#)Makefile	1.4	97/04/07 SMI"
#

TOP= .

include $(TOP)/Makefile.master

.KEEP_STATE:

MOD=via

OBJS=	via.o

MOD_CONF= $(MOD:%=%.conf)

all: $(MOD)

include $(TOP)/Makefile.driver

CFLAGS += -Wall -DDEBUG -D$(INST) $(ENVCPPFLAGS1) $(ENVCPPFLAGS2)

