/*
 * via.c --- VIA 82C686a AC97 audio driver - EXPERIMENTAL!!!
 * $Id:$
 * (C)2001 Henryk Paluch, paluch@bimbo.fjfi.cvut.cz
 * GNU LGPL license.
 * This code is losely based on many other driver sources:
 *
 * SoundBaster PCI 16,128 Solaris x86 driver
 *	 Written by Philip Brown <phil@bolthole.com>
 *	 Rewritten by J.rgen Keil <jk@tools.de>
 * Linux VIA82C686 driver:
 *      Copyright 1999,2000 Jeff Garzik <jgarzik@mandrakesoft.com>
 * FreeBSD VIA82C686 driver:
 *      Copyright (c) 2000 David Jones <dej@ox.org>
 * Sun Microsystems, Inc. pio.c driver
 */

#include <sys/errno.h>
#include <sys/conf.h>
#include <sys/modctl.h>
#include <sys/open.h>
#include <sys/stat.h>
#include <sys/ddi.h>
#include <sys/sunddi.h>
#include <sys/pci.h>
#include <sys/cmn_err.h>
#include <sys/audio.h>

#define MIN(x,y) ( (x) < (y) ? (x) : (y) )

#include "via_io.h"
#include "via_reg.h"

static	void *state_head;	/* opaque handle top of state structs */

/*
 * These are the entry points into our driver that are called when the
 * driver is loaded, during a system call, or in response to an interrupt.
 */
static	int	via_getinfo(dev_info_t *dip, ddi_info_cmd_t infocmd, void *arg,
		    void **result);
static	int	via_attach(dev_info_t *dip, ddi_attach_cmd_t cmd);
static	int	via_open(dev_t *dev, int openflags, int otyp, cred_t *credp);
static	int	via_close(dev_t dev, int openflags, int otyp, cred_t *credp);
static	int	via_write(dev_t dev, struct uio *uiop, cred_t *credp);
static	int	via_ioctl(dev_t dev, int cmd, intptr_t arg, int flags,
		    cred_t *credp, int *rvalp);
static	u_int	via_intr(caddr_t instance);
static	int	via_detach(dev_info_t *dip, ddi_detach_cmd_t cmd);

/*
 * When our driver is loaded or unloaded, the system calls our _init or
 * _fini routine with a modlinkage structure.  The modlinkage structure
 * contains:
 *
 *	modlinkage->
 *		modldrv->
 *			dev_ops->
 * 				cb_ops
 *
 * cb_ops contains the normal driver entry points and is roughly equivalent
 * to the cdevsw & bdevsw structures in previous releases.
 *
 * dev_ops contains, in addition to the pointer to cb_ops, the routines
 * that support loading and unloading our driver.
 */
static struct cb_ops	via_cb_ops = {
	via_open,
	via_close,
	nodev,			/* not a block driver	*/
	nodev,			/* no print routine	*/
	nodev,			/* no dump routine	*/
	nodev,			/* no read routine; write-only device */
	via_write,
	via_ioctl,
	nodev,			/* no devmap routine	*/
	nodev,			/* no mmap routine	*/
	nodev,			/* no segmap routine	*/
	nochpoll,		/* no chpoll routine	*/
	ddi_prop_op,
	0,			/* not a STREAMS driver	*/
	D_NEW | D_MP,		/* safe for multi-thread/multi-processor */
};

static struct dev_ops via_ops = {
	DEVO_REV,		/* devo_rev */
	0,			/* devo_refcnt */
	via_getinfo,		/* devo_getinfo */
	nulldev,		/* devo_identify */
	nulldev,		/* devo_probe */
	via_attach,		/* devo_attach */
	via_detach,		/* devo_detach */
	nodev,			/* devo_reset */
	&via_cb_ops,		/* devo_cb_ops */
	(struct bus_ops *)0,	/* devo_bus_ops */
	NULL,			/* devo_power */
};

extern	struct	mod_ops mod_driverops;

static	struct modldrv modldrv = {
	&mod_driverops,
	"VIA 82C686 audio driver 0.0",
	&via_ops,
};

static	struct modlinkage modlinkage = {
	MODREV_1,		/* MODREV_1 indicated by manual */
	(void *)&modldrv,
	NULL,			/* termination of list of linkage structures */
};

/*
 * _init, _info, and _fini support loading and unloading the driver.
 */
int
_init(void)
{
	int	error;

	if ((error = ddi_soft_state_init(&state_head, sizeof (Via), 1)) != 0)
		return (error);

	if ((error = mod_install(&modlinkage)) != 0)
		ddi_soft_state_fini(&state_head);

	return (error);
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

int
_fini(void)
{
	int status;

	if ((status = mod_remove(&modlinkage)) != 0)
		return (status);

	ddi_soft_state_fini(&state_head);

	return (status);
}
/*!!! DMA STUFF*/
static ddi_dma_attr_t dma_attr = {
    DMA_ATTR_V0,
    0,				/* addr_low */
    0xffffffff,			/* addr_high */
    0x03fffffc,			/* DMA counter register */
    1,				/* DMA address alignment */
    0x3c,			/* DMA burstsizes */
    4,				/* minxfer effective DMA size */
    0x03ffffff,			/* max DMA xfer size */
    0xffffffff,			/* segment boundary*/
    1,				/* s/g length */
				/*   (we want physically contigous mem) */
    4,				/* granularity of device */
				/*   (sbpci dma operates on longwords) */
    0				/* flags */
};
static void show_dma(Sdma *dma)
{
	cmn_err(CE_NOTE,"%s: vaddr(0x%p), paddr(0x%p), reallen(%d), plen(%d)",
		dma->name,dma->vaddr,dma->cookie.dmac_address,
		dma->reallen,dma->cookie.dmac_size);
}

static int alloc_dma(dev_info_t *dip,Sdma *dma,uint_t length)
{
	ddi_device_acc_attr_t	dev_acc_attr;
	int 			bindstat;
	int			len;
	/*
	 * Initialize the device access attributes for 
	 * DMA mapping
	 */
	dev_acc_attr.devacc_attr_version = DDI_DEVICE_ATTR_V0;
	dev_acc_attr.devacc_attr_endian_flags = DDI_STRUCTURE_LE_ACC;
	dev_acc_attr.devacc_attr_dataorder = DDI_STRICTORDER_ACC;

#ifdef DEBUG
	cmn_err(CE_NOTE, __FUNCTION__ " for %s",dma->name);
#endif
	/*
	 * Alloc DMA handle
         */
	    if (ddi_dma_alloc_handle(dip, &dma_attr,
			     DDI_DMA_DONTWAIT, NULL,
			     &dma->dma_handle) != DDI_SUCCESS)
		goto error0;
	  /*
           * Allocate physical memory and map it to VIRUTAL SPACE
	   */
	if (ddi_dma_mem_alloc(dma->dma_handle,
			      length,
			      &dev_acc_attr,
			      DDI_DMA_STREAMING,
			      DDI_DMA_DONTWAIT, NULL,
			      &(dma->vaddr),
			      &(dma->reallen),
			      &(dma->acc_handle)) != DDI_SUCCESS) 
		goto error1;
	/*
         * COOKIE to get physical address of DMA buffers...
	 */
	 bindstat = ddi_dma_addr_bind_handle(
		dma->dma_handle,
		NULL,
		dma->vaddr, dma->reallen,
		DDI_DMA_WRITE | DDI_DMA_STREAMING | DDI_DMA_REDZONE,
		DDI_DMA_DONTWAIT, NULL,
		&dma->cookie, &dma->ccount);
    if (bindstat != DDI_DMA_MAPPED) {
	cmn_err(CE_WARN,"Could not map DMA chunk at %p, bindstat=%d",
			      dma->vaddr, bindstat);
	if (bindstat == DDI_DMA_PARTIAL_MAP)
	    goto error3;

	/* else failure, do not unbind dma handle */
	goto error2;
	}

    if (dma->ccount > 1) {
	/* 
 	 * should not happen, because we've allocated physically
	 * contiguous ram
	 */
	cmn_err(CE_WARN,"More than one cookie (%d)", dma->ccount);
	for (len = 0; len < dma->ccount; len++) {
	    if (len > 0)
		ddi_dma_nextcookie(dma->dma_handle, &(dma->cookie));
	    cmn_err(CE_NOTE,"cookie #%d: paddr 0x%p, len 0x%lx",
		       len,
		       (caddr_t)dma->cookie.dmac_address,
		       (long)dma->cookie.dmac_size);
	}
	goto error3;
    }

#ifdef DEBUG
	show_dma(dma);
#endif
	return TRUE;
/* FAILED requests */
error3:
	// !!!: unbind does not accept pointers!!!
	ddi_dma_unbind_handle(dma->dma_handle);
error2:
	ddi_dma_mem_free(&dma->acc_handle);
error1:
	ddi_dma_free_handle(&dma->dma_handle);
error0:
	return FALSE;
}

static void free_dma(Sdma *dma)
{
	// !!!: unbind does not accept pointers!!!
	ddi_dma_unbind_handle(dma->dma_handle);
	ddi_dma_mem_free(&dma->acc_handle);
	ddi_dma_free_handle(&dma->dma_handle);
}

/*
 * Initializa VIA SGD (Offset 14) register to point to our SGD Table
 */
static void init_sgd4pointer(Via *via_p)
{
	uint32_t physaddr=via_p->sgdt_dma.cookie.dmac_address;

	ddi_put32(via_p->sgd_regs_handle,
		 (uint32_t*)(via_p->sgd_regs+VIA_SGD4_SGDT_POINTER),
		 physaddr);
}

static void dump_sgdts(Sgdt *sgdt)
{
	int i;
	for(i=0;i<N_FRAGS;i++)
	{
		cmn_err(CE_NOTE,"sgdt[%d]: base(0x%p), count(%d), flags(0x%x)",
			i,
			sgdt[i].base,sgdt[i].count & SGT_COUNT_MASK,
			sgdt[i].count & SGT_FLAGS_MASK);
	}
}
/*
 * Initialize Sgdt structures to point to buffer buf_dma
 */
static void init_sgdts(Via *via_p)
{
	int i;
	uint32_t physbuf=via_p->buf_dma.cookie.dmac_address;
	Sgdt *sgdt=(Sgdt*)(via_p->sgdt_dma.vaddr);
	ASSERT(via_p->sgdt_dma.reallen == SGDT_DMA_SIZE);
	ASSERT(via_p->buf_dma.reallen == BUF_DMA_SIZE);
	bzero(sgdt,SGDT_DMA_SIZE);
	for(i=0;i<N_FRAGS;i++)
	{
		sgdt[i].base=physbuf+(i*FRAG_SIZE);
		sgdt[i].count=FRAG_SIZE | SGT_BIT_FLAG;
	}
	sgdt[N_FRAGS-1].count |= SGT_BIT_EOL;
#ifdef DEBUG
	dump_sgdts(sgdt);
#endif
}
/*@@@*/
static int via_wait_ready(Via *via_p)
{
  uint32_t r;
  int i;
  for(i=1000;i>0;i--)
  {
	r = ddi_get32(via_p->sgd_regs_handle,
		       (uint32_t*)(via_p->sgd_regs+VIA_AC97_CTL));
        if ( (r & VIA_AC97_CTL_BUSY)==0 )
		break;
	drv_usecwait(100);
  }
#if 0
  cmn_err(CE_NOTE,__FUNCTION__ "ac97_ctl:0x%x, i=%d, busy: 0x%x",r,i,VIA_AC97_CTL_BUSY);
#endif
  if (i>0)
	return TRUE;
  else
  {
	cmn_err(CE_WARN,__FUNCTION__ "ac97_ctl:0x%x TIMEOUT!",r);
	return FALSE;
  }
}

static int via_wait_valid(Via *via_p)
{
  uint32_t r;
  int i;

  if (!via_wait_ready(via_p))
	return FALSE;

  for(i=1000;i>0;i--)
  {
	r = ddi_get32(via_p->sgd_regs_handle,
		       (uint32_t*)(via_p->sgd_regs+VIA_AC97_CTL));
        if ( (r & VIA_AC97_CTL_PRI_VALID)!=0 )
		break;
	drv_usecwait(100);
  }
#if 0
  cmn_err(CE_NOTE,__FUNCTION__ "ac97_ctl:0x%x, i=%d",r,i);
#endif
  if (i>0)
	return TRUE;
  else
  {
	cmn_err(CE_WARN,__FUNCTION__ "ac97_ctl:0x%x TIMEOUT!",r);
	return FALSE;
  }
}

static int via_read_codec(Via *via_p, uint16_t *outval, uint8_t reg)
{
	uint32_t w;
	if (!via_wait_ready(via_p))
		return FALSE;

	w= VIA_AC97_CTL_READ_MODE 
	   | VIA_AC97_CTL_PRI_VALID | VIA_AC97_CTL_INDEX(reg);

	ddi_put32(via_p->sgd_regs_handle,
		 (uint32_t*)(via_p->sgd_regs+VIA_AC97_CTL),
		 w);
	drv_usecwait(20);
	if (!via_wait_valid(via_p))
		return FALSE;

	*outval = ddi_get16(via_p->sgd_regs_handle,
		       (uint16_t*)(via_p->sgd_regs+VIA_AC97_CTL));
	// ACK primary codec valid read...
/*	ddi_put8(via_p->sgd_regs_handle,
		 (uint8_t*)(via_p->sgd_regs+0x83),
		 0x02); */
	return TRUE;
}

static int via_write_codec(Via *via_p, uint16_t data, uint8_t reg)
{
	uint32_t w;
	if (!via_wait_ready(via_p))
		return FALSE;

	w=  VIA_AC97_CTL_INDEX(reg) | data;
//	w|= (1<<25);

	ddi_put32(via_p->sgd_regs_handle,
		 (uint32_t*)(via_p->sgd_regs+VIA_AC97_CTL),
		 w);
	drv_usecwait(10);

	return via_wait_valid(via_p);
}

static int via_set_rate(Via *via_p, uint16_t rate)
{
	uint8_t reg=AC97_DAC_RATE;
	uint16_t statreg;
	int status;
	uint16_t dac_rate;

	/*
	 * Setting EXT_STATUS is absolutely necessary on my VIA 82C686B
	 * to enable Variable Rate !!!!
         */
	if (via_read_codec(via_p,&statreg,AC97_EXT_STATUS))
        {
		if ( (statreg&1)==0)
		{
#ifdef DEBUG
			cmn_err(CE_NOTE,"Not EXT_STATUS - trying to set");
#endif
			statreg |= 1;
			if (!via_write_codec(via_p,statreg,AC97_EXT_STATUS))
				cmn_err(CE_WARN,"Can not enable EXT_STATUS - probably locked codec");
		}
 	}
	else
		return FALSE;
#ifdef DEBUG
	cmn_err(CE_NOTE, __FUNCTION__ ": setting rate to: %d\n",(uint32_t)rate);
#endif
	status=via_write_codec(via_p,rate,reg);
	if (!status)
	{
		cmn_err(CE_WARN,__FUNCTION__ ": via_write_codec() failed");
		return FALSE;
	}

	status=via_read_codec(via_p,&dac_rate,AC97_DAC_RATE);
	if (status)
        {
#ifdef DEBUG
		cmn_err(CE_NOTE,"DAC rate: %dHz",
		dac_rate);
#endif
		if (dac_rate!=rate)
		{
			cmn_err(CE_WARN,"Unable to set DAC rate: %dHz instead of %dHz",
			dac_rate,rate);
			status=FALSE;
		}	
	}
	return status;
}

static int via_set_mixer(Via *via_p)
{
	uint16_t w;
	static uint8_t unmute[]={0x2,0x4,0x6,0xa,0xc,0xe,0x10,0x12,
		0x14,0x16,0x18,0x36,0x38};
	int i;

/*
	if (!via_write_codec(via_p,0,AC97_POWER_CONTROL))
		return FALSE;
	if (!via_write_codec(via_p,0,AC97_RESET))
		return FALSE;
	drv_usecwait(1000000);

	if (!via_write_codec(via_p,0,AC97_POWER_CONTROL))
		return FALSE;
*/

	for(i=0;i<sizeof(unmute)/sizeof(uint8_t);i++)
        {
		uint8_t reg=unmute[i];

		if (!via_read_codec(via_p,&w,reg))
			return FALSE;
//		w &= ~(0x8000);
		w = 0x0c0c;
		cmn_err(CE_NOTE,"reg(%d): %x",reg,w);
		if (!via_write_codec(via_p,w,reg))
			return FALSE;
		cmn_err(CE_NOTE,"reg(%d): %x",reg,w);
 	}
/*
//	w=0x0101;
	if (!via_read_codec(via_p,&w,AC97_MASTER_VOL_MONO))
		return FALSE;
	cmn_err(CE_NOTE,"MASTER MONO: %x",w);
//	w=0x01;
	w &= ~(0x8000);
	if (!via_write_codec(via_p,w,AC97_MASTER_VOL_MONO))
		return FALSE;
	cmn_err(CE_NOTE,"MASTER MONO: %x",w );

	if (!via_read_codec(via_p,&w,AC97_PCMOUT_VOL))
		return FALSE;
	cmn_err(CE_NOTE,"PCM OUT VOL: %x",w );
//	w=0x0101;
	w &= ~(0x8000);
	if (!via_write_codec(via_p,w,AC97_PCMOUT_VOL))
		return FALSE;
	cmn_err(CE_NOTE,"PCM OUT VOL: %x",w  );

	if (!via_read_codec(via_p,&w,AC97_HEADPHONE_VOL))
		return FALSE;
	cmn_err(CE_NOTE,"HEADPHONE OUT VOL: %x",w );
//	w=0x01;
	w &= ~(0x8000);
	if (!via_write_codec(via_p,w,AC97_HEADPHONE_VOL))
		return FALSE;
	cmn_err(CE_NOTE,"HEADPHONE OUT VOL: %x",w  );

	if (!via_read_codec(via_p,&w,AC97_AUX_VOL))
		return FALSE;
	cmn_err(CE_NOTE,"AUX_VOL VOL: %x",w );
//	w=0x01;
	w &= ~(0x8000);
	if (!via_write_codec(via_p,w,AC97_AUX_VOL))
		return FALSE;
	cmn_err(CE_NOTE,"AUX_VOL VOL: %x",w  );

*/
	if (!via_read_codec(via_p,&w,AC97_POWER_CONTROL))
		return FALSE;
	cmn_err(CE_NOTE,"POWER : %x",w );
	return TRUE;
}

static int via_show_rates(Via *via_p)
{
	uint16_t dac_rate;
	uint16_t adc_rate;
	if (!via_read_codec(via_p,&dac_rate,AC97_DAC_RATE))
		return FALSE;
	cmn_err(CE_NOTE,"DAC rate: %dHz",
		dac_rate);
	if (!via_read_codec(via_p,&adc_rate,AC97_ADC_RATE))
		return FALSE;
	cmn_err(CE_NOTE,"ADC rate: %dHz",
		adc_rate);
	return TRUE;
}

static void via_set_sgd_type(Via *via_p, uint8_t bit, int set)
{
	uint8_t b;
	b = ddi_get8(via_p->sgd_regs_handle,
		       (uint8_t*)(via_p->sgd_regs+VIA_SGD2_TYPE));
#ifdef DEBUG
	cmn_err(CE_NOTE,__FUNCTION__ ": request bit: 0x%x to %s",bit,
			( set ? "set" : "unset"));
	cmn_err(CE_NOTE,__FUNCTION__ ": OLD SGD2 TYPE: 0x%x",b);
#endif
	if (set)
		b |= bit;
	else    
		b &= ~bit;

	ddi_put8(via_p->sgd_regs_handle,
		 (uint8_t*)(via_p->sgd_regs+VIA_SGD2_TYPE),
		 b);

#ifdef DEBUG
	cmn_err(CE_NOTE,__FUNCTION__ ": NEW SGD2 TYPE: 0x%x",b);
#endif

}
/*
 * set output format:
 * fmt: SET_FMT_16 - 16bit data
 *      SET_FMT_8  - 8bit data
 */
static void via_set_fmt(Via *via_p,enum SET_FMT fmt)
{
	via_set_sgd_type(via_p,VIA_SGD2_TYPE_BITS, (fmt==SET_FMT_16));
}

/*
 * set output mode to STEREO or MONO
  *       SET_MONO
 *        SET_STEREO
 */
static void via_set_channels(Via *via_p,enum SET_CHANNELS mode)
{
	via_set_sgd_type(via_p,VIA_SGD2_TYPE_STEREO, (mode==SET_STEREO));

}

static void via_setctrl(Via *via_p,uint8_t set_bit)
{
	uint8_t b;
	b = ddi_get8(via_p->sgd_regs_handle,
		       (uint8_t*)(via_p->sgd_regs+VIA_SGD1_CONTROL));
	// just to be sure
	b &= ~(VIA_SGD1_BIT_STOP | VIA_SGD1_BIT_START);
	b |= set_bit;
	ddi_put8(via_p->sgd_regs_handle,
		 (uint8_t*)(via_p->sgd_regs+VIA_SGD1_CONTROL),
		 b);
	cmn_err(CE_NOTE,__FUNCTION__ "0x%x",b);
}

static void via_play(Via *via_p)
{
	via_set_sgd_type(via_p,
		VIA_SGD2_TYPE_IEOL
                | VIA_SGD2_TYPE_IFLAG
                | VIA_SGD2_TYPE_AUTOSTARTEOL,
                1);
	via_setctrl(via_p,VIA_SGD1_BIT_START);
	via_p->playing=1;
}

static void via_stop(Via *via_p)
{
	via_setctrl(via_p,VIA_SGD1_BIT_STOP);
	via_p->playing=0;
}


static void show_pci_bases(ddi_acc_handle_t conf_handle)
{
#define N_MAPS 6
	uint8_t  map[N_MAPS]={PCI_CONF_BASE0,PCI_CONF_BASE1,PCI_CONF_BASE2,
			PCI_CONF_BASE3,PCI_CONF_BASE4,PCI_CONF_BASE5};
	int i;
	for(i=0;i<N_MAPS;i++)
        {
	    uint32_t pci_base;
	    pci_base    = pci_config_get32(conf_handle, map[i]);
		cmn_err(CE_NOTE,"base%d:0x%x\n",i,pci_base);
	}
#undef N_MAPS
}

/*
 * enable bus mastering for pci card
 * XXX: according to VIA manual not needed (BM is fixed 0)
 */
static void via_enable_busmaster(ddi_acc_handle_t conf_handle)
{
    uint16_t pci_commandreg;
    pci_commandreg = pci_config_get16(conf_handle, PCI_CONF_COMM);
#ifdef DEBUG
    cmn_err(CE_NOTE, "pci command register 0x%x %s%s%s%s",
	       pci_commandreg,
	       pci_commandreg & PCI_COMM_SERR_ENABLE ? " serr#_en"  : "",
	       pci_commandreg & PCI_COMM_ME  ? " busmaster" : "",
	       pci_commandreg & PCI_COMM_MAE ? " mem_access" : "",
	       pci_commandreg & PCI_COMM_IO  ? " io_access" : "");
#endif
    if (pci_commandreg & PCI_COMM_ME) {
	cmn_err(CE_NOTE,"Busmastering already enabled");
    } else {
	cmn_err(CE_NOTE,"Enabling PCI Bus mastering");
	pci_config_put16(conf_handle, PCI_CONF_COMM, pci_commandreg | PCI_COMM_ME);
    }
}

static int
via_pci_setup(Via *via_p)
{
    uint16_t pci_vendorid;
    uint16_t pci_deviceid;
    uint8_t  pci_revision;
    uint8_t  pci_intrinfo;
    uint32_t pci_base0;
    ddi_acc_handle_t conf_handle;
    int detected=FALSE;

    if (pci_config_setup(via_p->dip, &conf_handle) != DDI_SUCCESS) {
	cmn_err(CE_WARN,"pci_config_setup() failed\n");
	return 0;
    }
    pci_vendorid = pci_config_get16(conf_handle, PCI_CONF_VENID);
    pci_deviceid = pci_config_get16(conf_handle, PCI_CONF_DEVID);
    pci_revision = pci_config_get8 (conf_handle, PCI_CONF_REVID);
    pci_intrinfo = pci_config_get8 (conf_handle, PCI_CONF_ILINE);
    pci_base0    = pci_config_get32(conf_handle, PCI_CONF_BASE0);

#ifdef DEBUG
    cmn_err(CE_NOTE,"enumerating vendor: 0x%x, device: 0x%x, rev: 0x%x\n",
	pci_vendorid, pci_deviceid, pci_revision);
    show_pci_bases(conf_handle);
#endif

    if(pci_vendorid==VIA_AUDIO_VENDORID && pci_deviceid==VIA_AUDIO_DEVICEID)
    {
         detected=TRUE;
	 cmn_err(CE_CONT, "VIA82C686 audio: %s, via%d, rev. 0x%02x) at 0x%x irq %d\n",
	    ddi_node_name(via_p->dip),
	    ddi_get_instance(via_p->dip), 
	    pci_revision,
	    pci_base0,
	    pci_intrinfo);
    /* 	via_enable_busmaster(conf_handle); */
    }

    pci_config_teardown(&conf_handle);
    return detected;
}

static void via_ac_link_setup(Via* via_p)
{
	uint8_t b;

	ddi_put8(via_p->pci_regs_handle,
		       (uint8_t*)(via_p->pci_regs+VIA_ACLINK_CTRL),
			VIA_CR41_AC97_ENABLE |
                               VIA_CR41_AC97_RESET | VIA_CR41_AC97_WAKEUP);
	drv_usecwait(100);
	ddi_put8(via_p->pci_regs_handle,
		       (uint8_t*)(via_p->pci_regs+VIA_ACLINK_CTRL),
                               0);

	drv_usecwait(100);

	ddi_put8(via_p->pci_regs_handle,
		       (uint8_t*)(via_p->pci_regs+VIA_ACLINK_CTRL),
			       VIA_CR41_AC97_ENABLE | VIA_CR41_PCM_ENABLE |
                               VIA_CR41_VRA | VIA_CR41_AC97_RESET);

	/* route FM trap to IRQ, disable FM trap */
	ddi_put8(via_p->pci_regs_handle,
		       (uint8_t*)(via_p->pci_regs+0x48),
                               0x05);
	drv_usecwait(10);
	ddi_put32(via_p->sgd_regs_handle,
		       (uint32_t*)(via_p->sgd_regs+0x8c),
                               0x0);

/*
	b = ddi_get8(via_p->pci_regs_handle,
		       (uint8_t*)(via_p->pci_regs+VIA_AC97_LINK_CTL));
	cmn_err(CE_NOTE,"OLD ac97(LINK_CTL):0x%x",b);

//	b &= ~( VIA_ACLINK_PCM_OUT_ENABLE);
 //       b |= ( VIA_ACLINK_VRA_ENABLE|4);
        b=0xcc;

	ddi_put8(via_p->pci_regs_handle,
		       (uint8_t*)(via_p->pci_regs+VIA_AC97_LINK_CTL),
			b);

	b = ddi_get8(via_p->pci_regs_handle,
		       (uint8_t*)(via_p->pci_regs+VIA_AC97_LINK_CTL));
	cmn_err(CE_NOTE,"NEW ac97(LINK_CTL):0x%x",b);

*/
	b = ddi_get8(via_p->pci_regs_handle,
		       (uint8_t*)(via_p->pci_regs+VIA_CR42_FUNCTION));
	cmn_err(CE_NOTE,"OLD (CR42_FUNCTION):0x%x",b);
	b &= ~(0xf);

	ddi_put8(via_p->pci_regs_handle,
		       (uint8_t*)(via_p->pci_regs+VIA_CR42_FUNCTION),
			b);
	cmn_err(CE_NOTE,"NEW (CR42_FUNCTION):0x%x",b);
}
static void via_test(Via* via_p)
{
	uint32_t r;
	uint8_t b;

	b = ddi_get8(via_p->pci_regs_handle,
		       (uint8_t*)(via_p->pci_regs+VIA_AC97_IF_STATUS));
	cmn_err(CE_NOTE,"ac97(IF_STATUS):0x%x",b);
	b = ddi_get8(via_p->pci_regs_handle,
		       (uint8_t*)(via_p->pci_regs+VIA_ACLINK_CTRL));
	cmn_err(CE_NOTE,"ac97(LINK_CTL):0x%x",b);
	r = ddi_get32(via_p->pci_regs_handle,
		       (uint32_t*)(via_p->pci_regs+VIA_AC97_CTL));

	cmn_err(CE_NOTE,"ac97(PCI):0x%x",r);
	r = ddi_get32(via_p->sgd_regs_handle,
		       (uint32_t*)(via_p->sgd_regs+VIA_AC97_CTL));
	cmn_err(CE_NOTE,"ac97(IOBASE0):0x%x",r);
}
/*
 * return TRUE if
 * vendor ID and Device ID are constistent in
 * mapped pci access
 * (viap->pci_regs_handle)
 */
static int via_check_pci_map(Via* via_p)
{
	uint16_t pci_vendorid;
	uint16_t pci_deviceid;
	pci_vendorid = ddi_get16(via_p->pci_regs_handle,
		       (uint16_t*)(via_p->pci_regs+PCI_CONF_VENID));
	pci_deviceid = ddi_get16(via_p->pci_regs_handle,
		       (uint16_t*)(via_p->pci_regs+PCI_CONF_DEVID));
#ifdef DEBUG
	cmn_err(CE_NOTE,"vendorid: 0x%x, deviceid: 0x%x,"
			" returned via mapped space\n",
			pci_vendorid, pci_deviceid);
#endif
	if (pci_deviceid==VIA_AUDIO_DEVICEID
		&& pci_vendorid==VIA_AUDIO_VENDORID)
		return TRUE;
	else
		return FALSE;
}
/*
 * Attach an instance of the driver.  We take all the knowledge we
 * have about our board and check it against what has been filled in for
 * us from our FCode or from our driver.conf(4) file.
 */
static int
via_attach(dev_info_t *dip, ddi_attach_cmd_t cmd)
{
	int			instance;
	Via			*via_p;
	int 			status;
	int 			nregs; /* number of register sets */
	ddi_device_acc_attr_t	dev_acc_attr;

#ifdef DEBUG
	cmn_err(CE_NOTE,"Entering" __FUNCTION__ "\n");
#endif
	switch (cmd) {
		case DDI_ATTACH:
			break;

		default:
			return (DDI_FAILURE);
	}

	instance = ddi_get_instance(dip);
#ifdef DEBUG
	cmn_err(CE_NOTE,"Got instance #%d\n",instance);
	cmn_err(CE_NOTE,"Calling ddi_soft_state_zalloc()\n");
#endif

	if (ddi_soft_state_zalloc(state_head, instance) != 0)
	{
		cmn_err(CE_WARN,"ddi_soft_state_zalloc() failed\n");
		goto error0;
	}

	via_p = (Via *) ddi_get_soft_state(state_head, instance);
	ddi_set_driver_private(dip, (caddr_t)via_p);
	via_p->dip = dip;

	if (!via_pci_setup(via_p))
	{
		cmn_err(CE_WARN,"via_pci_setup() failed\n");
		goto error1;
	}
	/*
	 * we only use one interrupt level, level 0
	 *
	 * We make no attempt to handle hi-level interrupts.
	 */
	if (ddi_intr_hilevel(dip, 0)) {
		cmn_err(CE_WARN,"ddi_intr_hilevel(dip,0) failed,"
				"ensure that hilevel is disabled in"
				".conf \n");
		goto error1;
	}

	/*
	 * Before adding the interrupt, get the interrupt block
	 * cookie associated with the interrupt specification, to
	 * be used to initialize the mutex used by the interrupt
	 * handler.
	 */
	if (ddi_get_iblock_cookie(dip, 0,
				&via_p->iblock_cookie) != DDI_SUCCESS) {
		cmn_err(CE_WARN,"ddi_get_iblock_cookie() failed -"
				"unable to get interrrupt\n");
		goto error1;
	}

	mutex_init(&via_p->mutex, "via mutex",
			MUTEX_DRIVER, (void *)via_p->iblock_cookie);

	/*
	 * Now that the mutex is initialized, we can add
	 * the interrupt itself.
	 */
	if (ddi_add_intr(dip, 0, &via_p->iblock_cookie,
				(ddi_idevice_cookie_t *)NULL, via_intr,
				(caddr_t)instance) != DDI_SUCCESS) {
		cmn_err(CE_WARN,"ddi_add_intr() failed - can not"
				"add interrupt handler\n");
		goto error2;
	}

	if ( ddi_dev_nregs(dip,&nregs)!=DDI_SUCCESS )
	{
		cmn_err(CE_WARN,"ddi_dev_nregs() failed\n");
		goto error2;
	}
#ifdef DEBUG
	cmn_err(CE_NOTE,"nregs=%d\n",nregs);
#endif
	if (nregs<4)
	{
		cmn_err(CE_WARN,"Only %d register spaces, must be 4\n",
				nregs);
		goto error2;
	}
	/*
	 * Initialize the device access attributes for the register
	 * mapping
	 */
	dev_acc_attr.devacc_attr_version = DDI_DEVICE_ATTR_V0;
	dev_acc_attr.devacc_attr_endian_flags = DDI_STRUCTURE_LE_ACC;
	dev_acc_attr.devacc_attr_dataorder = DDI_STRICTORDER_ACC;

	/*
	 * We need to map PCI space to get access to some special
	 * registers - AC97 Interface, AC Link Interface Control
	 *
	 * XXX: It seems that VIA manual is a bit broken here.
	 *      According to Linux & FreeBSD sources and few experiments
         *      these registers are mapped in IOBASE0 space !!!
	 */
	status = ddi_regs_map_setup(
			dip,
			VIA_MAP_INDEX_PCI,
			&(via_p->pci_regs),
			VIA_OFFSET_PCI,
			VIA_LENGTH_PCI,
			&dev_acc_attr,
			&(via_p->pci_regs_handle)
			);

	if (status != DDI_SUCCESS)
	 {
		if (status == DDI_REGS_ACC_CONFLICT)
			cmn_err(CE_WARN,
			 "PCI regs mapping conflicted with other mapping");
                else
			cmn_err(CE_WARN, "PCI regs mapping failed");
		goto error3;
	}

	if (!via_check_pci_map(via_p))
	{
		cmn_err(CE_WARN,"PCI regs mapping check failed\n");
		goto error4;
	}

	/*
	 * map SGD (Scatter Gather DMA) IOBASE0 registers
	 */
	status = ddi_regs_map_setup(dip,
			VIA_MAP_INDEX_SGD,
			&(via_p->sgd_regs),
			VIA_OFFSET_SGD,
			VIA_LENGTH_SGD,
			&dev_acc_attr,
			&(via_p->sgd_regs_handle));
	if (status != DDI_SUCCESS) {
		if (status == DDI_REGS_ACC_CONFLICT) {
			cmn_err(CE_WARN, "SGD regs mapping conflicted with other mapping");
		} else {
			cmn_err(CE_WARN, "SGD regs mapping failed");
		}
		goto error4;
	}

	strcpy(via_p->sgdt_dma.name,"sgdt_dma");
	strcpy(via_p->buf_dma.name,"buf_dma");
        if(!alloc_dma(dip,&via_p->sgdt_dma,SGDT_DMA_SIZE))
		goto error5;
        if(!alloc_dma(dip,&via_p->buf_dma,BUF_DMA_SIZE))
		goto error6;

	init_sgdts(via_p);
	/*
	 * ddi_create_minor_node creates an entry in an internal kernel
	 * table; the actual entry in the file system is created by
	 * drvconfig(1) when you run add_drv(1);
	 */
	if (ddi_create_minor_node(dip, "sound,audio", S_IFCHR, instance,
				DDI_NT_AUDIO, 0) == DDI_FAILURE) {
		cmn_err(CE_WARN,"ddi_create_minor_node() failed\n");
		goto error7;
	}

	ddi_report_dev(dip);

	via_ac_link_setup(via_p);
	via_set_mixer(via_p);
#ifdef DEBUG
	via_test(via_p);
	via_show_rates(via_p);
	cmn_err(CE_NOTE, __FUNCTION__ ": SUCCESS\n");
#endif
	return (DDI_SUCCESS);
	/* error returns */
error7:
	free_dma(&via_p->buf_dma);
error6:
	free_dma(&via_p->sgdt_dma);
error5:
	ddi_regs_map_free(&(via_p->sgd_regs_handle));
error4:
	ddi_regs_map_free(&(via_p->pci_regs_handle));
error3:
	ddi_remove_intr(via_p->dip, 0, via_p->iblock_cookie);
error2:
	mutex_destroy(&via_p->mutex);
error1:
	ddi_soft_state_free(state_head, instance);
error0:
	return (DDI_FAILURE);
}

/*
 * This is a pretty generic getinfo routine as describe in the manual.
 */
/*ARGSUSED*/
static int
via_getinfo(dev_info_t *dip, ddi_info_cmd_t infocmd, void *arg, void **result)
{
	int 	error;
	Via	*via_p;

	switch (infocmd) {
	case DDI_INFO_DEVT2DEVINFO:
		via_p = (Via *) ddi_get_soft_state(state_head,
			    getminor((dev_t)arg));
		if (via_p == NULL) {
			*result = NULL;
			error = DDI_FAILURE;
		} else {
			mutex_enter(&via_p->mutex);
			*result = via_p->dip;
			mutex_exit(&via_p->mutex);
			error = DDI_SUCCESS;
		}
		break;
	case DDI_INFO_DEVT2INSTANCE:
		*result = (void *)getminor((dev_t)arg);
		error = DDI_SUCCESS;
		break;
	default:
		*result = NULL;
		error = DDI_FAILURE;
	}

	return (error);
}


/*
 * When our driver is unloaded, via_detach cleans up and frees the
 * resources we allocated in via_attach.
 */
static int
via_detach(dev_info_t *dip, ddi_detach_cmd_t cmd)
{
	Via		*via_p;
	int		instance;

	switch (cmd) {
	case DDI_DETACH:
		break;

	default:
		return (DDI_FAILURE);
	}

	instance = ddi_get_instance(dip);
	via_p = (Via *) ddi_get_soft_state(state_head, instance);


	/*
	 * remove all alocated resources
	 */
	ddi_remove_minor_node(dip, NULL);

	free_dma(&via_p->buf_dma);
	free_dma(&via_p->sgdt_dma);

	ddi_regs_map_free(&(via_p->sgd_regs_handle));
	ddi_regs_map_free(&(via_p->pci_regs_handle));
	ddi_remove_intr(via_p->dip, 0, via_p->iblock_cookie);
	mutex_destroy(&via_p->mutex);
	ddi_soft_state_free(state_head, instance);

	return (DDI_SUCCESS);
}

/*
 * via_open is called in response to the open(2) system call
 */
/*ARGSUSED*/
static	int
via_open(dev_t *dev, int openflags, int otyp, cred_t *credp)
{
	int	retval = 0;
	Via	*via_p;

	via_p = (Via *) ddi_get_soft_state(state_head, getminor(*dev));

	/*
	 * Verify instance structure
	 */
	if (via_p == NULL)
		return (ENXIO);

	/*
	 * Verify we are being opened as a character device
	 */
	if (otyp != OTYP_CHR)
		return (EINVAL);

	/*
	 * lock control status register and exclusive open flag
	 */
	mutex_enter(&via_p->mutex);

	/*
	 * Only allow a single open
	 */
	if (via_p->via_state == VIA_OPEN) {
		retval = EBUSY;
	} else {
		if (via_set_rate(via_p,8000))
		{
			via_set_channels(via_p,SET_MONO);
			via_set_fmt(via_p,SET_FMT_8);
			/*
			 * mark device as open
			 */
			via_p->via_state |= VIA_OPEN;
			/*
		         * we don't start play until buffers are full...
			 */
			via_p->playing=0;
			init_sgd4pointer(via_p);

			// index of 1st free fragment
			via_p->free_index=0;
			// number of free fragments
			via_p->n_free=N_FRAGS;
			// number of available space in current fragment
			via_p->frag_avail=FRAG_SIZE;
			
			/*
			 * start device except for interrupts
			 */
		}
	}

	mutex_exit(&via_p->mutex);

	return (retval);
}

/*
 * via_close is called after the last process that has the device open
 * calls close(2)
 *
 * since we enforce exclusive open, via_close will always be called when
 * the user process calls close(2)
 */
/*ARGSUSED*/
static	int
via_close(dev_t dev, int openflags, int otyp, cred_t *credp)
{
	Via	*via_p;

	via_p = (Via *) ddi_get_soft_state(state_head, getminor(dev));

	/*
	 * lock control status register and exclusive open flag
	 */
	mutex_enter(&via_p->mutex);

	/*
	 * Wait until buffer empty...
	 */
/* NEED TO DEBUG (causes locks)
	while(via_p->playing)
	{
		cv_wait(&via_p->cv, &via_p->mutex);
	}	
*/
	via_stop(via_p);

	/*
	 * let the next person open the device
	 */
	via_p->via_state &= ~VIA_OPEN;

	mutex_exit(&via_p->mutex);

	return (0);
}

#define SHOW_FRAGS do{  \
	cmn_err(CE_NOTE, __FUNCTION__ "line: %d: free_index(%d),n_free(%d),frag_avail(%d), resid(%d)", __LINE__ , via_p->free_index,via_p->n_free,via_p->frag_avail,uiop->uio_resid); \
}while(0); 


/*ARGSUSED*/
static	int
via_write(dev_t dev, struct uio *uiop, cred_t *credp)
{
	int	error = OK;
	Via	*via_p;
	uint32_t tmp;

	via_p = (Via *) ddi_get_soft_state(state_head, getminor(dev));

	/*
	 * lock control status register and data register
	 */
	mutex_enter(&via_p->mutex);

/*
	tmp = ddi_get32(via_p->sgd_regs_handle,
		       (uint32_t*)(via_p->sgd_regs+0x14));
	cmn_err(CE_NOTE,"Counter pointer: 0x%x",tmp);
*/
	
	if (via_p->n_free==0)
	{
		if (!via_p->playing)
		{
			via_play(via_p);
		}
		/*
		 * wait for interrupt (see via_intr)
		 *
		 * Note that we currently hold the MUTEX.  cv_wait will
		 * temporarily drop the MUTEX so that via_intr can
		 * acquire it; via_intr drops the MUTEX after calling
		 * cv_signal, and cv_wait re-acquires it for us before
		 * it returns.
		 *
		 * Note to 4.x developers: this use of cv_wait/cv_signal
		 * within a MUTEX is similar to the use of sleep/wakeup
		 * within an spl.  There are, however, important differences.
		 * MUTEXes lock data structures; they do not block processor
		 * interrupts.  Also, the interrupt routine can block waiting
		 * for a MUTEX.
		 */
		cv_wait(&via_p->cv, &via_p->mutex);

/*
	tmp = ddi_get32(via_p->sgd_regs_handle,
		       (uint32_t*)(via_p->sgd_regs+0x14));
	cmn_err(CE_NOTE,"Counter pointer: 0x%x",tmp);
	
	tmp = ddi_get32(via_p->sgd_regs_handle,
		       (uint32_t*)(via_p->sgd_regs+0x1c));
	cmn_err(CE_NOTE,"Counter: %d",tmp);
*/
	}
//	SHOW_FRAGS;
	/* Fill in buffers as much as possible... */
	while(via_p->n_free>0 && uiop->uio_resid>0)
	{
		uint32_t length;
		uint32_t toVaddr;
//		SHOW_FRAGS; 
		if(via_p->frag_avail==0)
		{
			via_p->n_free--;
			via_p->frag_avail=FRAG_SIZE;
			via_p->free_index++;
			if (via_p->free_index>=N_FRAGS)
				via_p->free_index=0;
			continue;
		}
		// copy userspace data to DMA buffers, ehm...
		length=MIN(uiop->uio_resid,via_p->frag_avail);
		toVaddr=via_p->buf_dma.vaddr+((via_p->free_index)*FRAG_SIZE
					     +(FRAG_SIZE-via_p->frag_avail));
//		cmn_err(CE_NOTE,"move: toVaddr(0x%x),length(%d)",
//			toVaddr,length);
		error=uiomove(
			toVaddr,
			length,
			UIO_WRITE,
			uiop);
		if (error)
			break;
//		ddi_dma_sync(via_p->sgdt_dma.dma_handle,0,via_p->sgdt_dma.reallen,DDI_DMA_SYNC_FORCPU);
//		ddi_dma_sync(via_p->buf_dma.dma_handle,0,via_p->buf_dma.reallen,DDI_DMA_SYNC_FORCPU);
		via_p->frag_avail-=length;
	}

	mutex_exit(&via_p->mutex);

	return (error);
}

/*
 * This is a sample ioctl routine to show the use of ddi_copyin(9F)
 * and ddi_copyout(9F).
 *
 * Note to 4.x developers: in previous releases, up to 128 bytes of data
 * were copied for you into the area pointed to by "arg".  This is no
 * longer true.  You must always use ddi_copyin(9F) or ddi_copyout(9F);
 * you can no longer simply dereference "arg".
 */

/*ARGSUSED*/
static	int
via_ioctl(dev_t dev, int cmd, intptr_t arg, int flags,
	cred_t *credp, int *rvalp)
{
	Via		*via_p;
	int		retval = 0;
	audio_info_t	audio_info;

	via_p = (Via *) ddi_get_soft_state(state_head, getminor(dev));

	mutex_enter(&via_p->mutex);
	switch (cmd) {
	case AUDIO_DRAIN:
		cmn_err(CE_WARN,"AUDIO_DRAIN - not yet implemented :-)");
		retval=0;
		break;

	case AUDIO_SETINFO:
		if (ddi_copyin((void *)arg,
			       &audio_info,
                               sizeof (audio_info_t),
		               flags) != 0) {
			retval = EFAULT;
		} else {
			uint_t speed=audio_info.play.sample_rate;
			uint_t channels=audio_info.play.channels;
			uint_t bits=audio_info.play.precision;
#ifdef DEBUG
			cmn_err(CE_NOTE,"SETINFO request: speed(%dHz), channels(%d),bits(%d)",speed,channels,bits);
#endif
			retval=EINVAL;
			// !!! Don't allow changes while we are playing
			//     sound ...
			if (via_p->playing)
			{
				retval=EBUSY;
				break;
			}
			if (speed<8000 || speed>48000)
				break;
			if (bits!=8 && bits!=16)
				break;
			if (channels<1 || channels>2)
				break;
			if (!via_set_rate(via_p,speed))
				break;
			via_set_fmt( via_p,
                                     (bits==16) ? SET_FMT_16 : SET_FMT_8 );
			via_set_channels( via_p,
				          (channels==2) ? SET_STEREO : SET_MONO);
			retval=0;
		}
		break;
	default:
		retval = EINVAL;
		break;
	}

	mutex_exit(&via_p->mutex);
	return (retval);
}

/*
 * Whenever the system sees an interrupt at the level we specified with
 * add_intr in via_attach, we get a chance to determine if it is our
 * interrupt or somebody else's.
 *
 * This routine will get called for each instance of each driver that uses
 * that level interrupt until somebody returns DDI_INTR_CLAIMED.
 *
 * Note to 4.x developers: in 4.x your xxpoll() routine had to check each
 * of your instances (units) to determine if any were interrupting.  This
 * is no longer necessary as the system now calls your xxintr() routine
 * for each attached instance of your device.
 */
static u_int
via_intr(caddr_t instance)
{
	Via		*via_p;
	u_int  		int_serviced = DDI_INTR_UNCLAIMED;

	via_p = (Via *) ddi_get_soft_state(state_head, (u_int)instance);

	/*
	 * lock the control status register
	 */
	mutex_enter(&via_p->mutex);

	if (via_p->playing ) {
		uint8_t b;
		b = ddi_get8(via_p->sgd_regs_handle,
				(uint8_t*)(via_p->sgd_regs+VIA_SGD0_STATUS));
		b &= STATUS_STOP | STATUS_EOL | STATUS_FLAG;
		if (b!=0)
		{
			/*
			 * we are interrupting
			 */
			int_serviced = DDI_INTR_CLAIMED;
			/*
			 * ACK interrupt
			 */

			ddi_put8(via_p->sgd_regs_handle,
					(uint8_t*)(via_p->sgd_regs+VIA_SGD0_STATUS),
					b);
			if (b&STATUS_FLAG)
			{
				// block is done - adjust frag buffer
				if (via_p->n_free>=N_FRAGS)
				{
					via_stop(via_p);
				}
				else
				{
					via_p->n_free++;

					/*
					 * "wake up" the top half
					 */
					cv_signal(&via_p->cv);
				}
			}

		}
	}

	mutex_exit(&via_p->mutex);

	return (int_serviced);
}
