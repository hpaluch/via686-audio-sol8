
#ifndef	_VIA_IO_H
#define	_VIA_IO_H

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * Vendor and device id vo VIA 82C686 audio
 * - can be read from PCI space
 */
#define VIA_AUDIO_VENDORID 0x1106
#define VIA_AUDIO_DEVICEID 0x3058
/*
 *  "number" in ddi_regs_map_setup()
 *   describes register set
 *   VIA has 3 I/O space sets described as IOBASE{0,1,2}
 */
#define VIA_MAP_INDEX_PCI  0

/* IOBASE0 */
#define VIA_MAP_INDEX_SGD  1
/* IOBASE1 */
#define VIA_MAP_INDEX_IOBASE1  2
/* IOBASE2 */
#define VIA_MAP_INDEX_IOBASE2  3

#define VIA_PCI_FUNCTION 0x42
/*
 * offset of PCI space we want to allocate
 */
#define VIA_OFFSET_PCI 0x00
#define VIA_OFFSET_SGD 0x00
/*
 * length of PCI space we need to access
 */
#define VIA_LENGTH_PCI 0x90
#define VIA_LENGTH_SGD 0x90

#define VIA_AC97_CTL   0x80
#define VIA_AC97_IF_STATUS   0x40
#define VIA_CR42_FUNCTION    0x42

#define VIA_SGD0_STATUS	0x0
#define STATUS_STOP		( 1 << 2)
#define STATUS_EOL		( 1 << 1)
#define STATUS_FLAG		( 1 << 0)

#define VIA_SGD1_CONTROL	0x1
#define VIA_SGD1_BIT_START	( 1 << 7)
#define VIA_SGD1_BIT_STOP	( 1 << 6)

#define VIA_SGD2_TYPE		0x2
#define VIA_SGD2_TYPE_BITS   ( 1 << 5 )
#define VIA_SGD2_TYPE_STEREO   ( 1 << 4 )
#define VIA_SGD2_TYPE_IEOL   ( 1 << 1 )
#define VIA_SGD2_TYPE_IFLAG   ( 1 << 0 )
#define VIA_SGD2_TYPE_AUTOSTARTEOL   ( 1 << 7 )

#define VIA_SGD4_SGDT_POINTER	0x4
/*
 * values for via_set_stereo()
 */
enum SET_CHANNELS { SET_MONO=0, SET_STEREO};

/*
 * values for via_set_fmt()
 */
enum SET_FMT {SET_FMT_8=0, SET_FMT_16};

#define VIA_ACLINK_VRA_ENABLE    0x08
#define VIA_ACLINK_PCM_OUT_ENABLE    0x01

#define VIA_AC97_CTL                 0x80
#define         VIA_AC97_CTL_READ_MODE           ( 1   << 23 )
#define         VIA_AC97_CTL_BUSY                ( 1   << 24 )
#define         VIA_AC97_CTL_PRI_VALID           ( 1   << 25 )
#define         VIA_AC97_CTL_INDEX(x)            ( (uint32_t)(x) << 16 )

/*
 * These looks like real AC97 registers - not VIA specific
 */
#define  AC97_RESET 0x0
#define  AC97_MASTER_VOL_STEREO  0x0002      // Line Out   
#define  AC97_HEADPHONE_VOL      0x0004      //      
#define  AC97_MASTER_VOL_MONO    0x0006      // TAD Output 
#define  AC97_AUX_VOL            0x0016      // Aux Input (stereo) 
#define  AC97_PCMOUT_VOL         0x0018  
#define  AC97_POWER_CONTROL      0x0026     
#define AC97_MUTE                0x8000

#define		AC97_EXT_STATUS	0x2a
#define		AC97_DAC_RATE	0x2c
#define		AC97_ADC_RATE	0x32

#define AC97_CENTER_LFE_MASTER  0x0036 
#define AC97_SURROUND_MASTER    0x0038  

#define VIA_ACLINK_CTRL		0x41
#define VIA_CR41_AC97_ENABLE	0x80 /* enable AC97 codec */
#define VIA_CR41_AC97_RESET	0x40 /* clear bit to reset AC97 */
#define VIA_CR41_AC97_WAKEUP	0x20 /* wake up from power-down mode */
#define VIA_CR41_AC97_SDO	0x10 /* force Serial Data Out (SDO) high */
#define VIA_CR41_VRA		0x08 /* enable variable sample rate */
#define VIA_CR41_PCM_ENABLE	0x04 /* AC Link SGD Read Channel PCM Data Output */
#define VIA_CR41_FM_PCM_ENABLE	0x02 /* AC Link FM Channel PCM Data Out */
#define VIA_CR41_SB_PCM_ENABLE	0x01 /* AC Link SB PCM Data Output */
#define VIA_CR41_BOOT_MASK	(VIA_CR41_AC97_ENABLE | \
				 VIA_CR41_AC97_WAKEUP | \
				 VIA_CR41_AC97_SDO)
#define VIA_CR41_RUN_MASK	(VIA_CR41_AC97_ENABLE | \
				 VIA_CR41_AC97_RESET | \
				 VIA_CR41_VRA | \
				 VIA_CR41_PCM_ENABLE)



#ifdef	__cplusplus
}
#endif

#endif	/* _VIA_IO_H */
