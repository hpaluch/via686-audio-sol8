#ifndef	_VIA_REG_H
#define	_VIA_REG_H

#ifdef	__cplusplus
extern "C" {
#endif


#ifdef i386
#define	VIA_NAME	"via"		/* name of the device we support */
#else
#define	VIA_NAME	"SUNW,via"	/* name string in FCode prom */
#endif

#define N_FRAGS 8
#define FRAG_SIZE 32768
#define BUF_DMA_SIZE (N_FRAGS*FRAG_SIZE)

#define SGT_BIT_EOL		( 1 << 31)
#define SGT_BIT_FLAG		( 1 << 30)
#define SGT_BIT_STOP		( 1 << 29)
#define SGT_COUNT_MASK		0x0fffffff
#define SGT_FLAGS_MASK		0xf0000000

typedef struct sgdt{
	uint32_t base;
	uint32_t count;
} Sgdt;

typedef struct sdma{
	char			name[16];
	ddi_dma_handle_t	dma_handle;
	caddr_t			vaddr; /* DMA memory VIRTUAL ADDRESS (accessible directly from kernel */
	size_t			reallen;
	ddi_acc_handle_t	acc_handle;
	ddi_dma_cookie_t	cookie;
	uint_t			ccount; // cookie coount
} Sdma;
#define SGDT_DMA_SIZE (sizeof(Sgdt)*N_FRAGS)

typedef struct via {				/* per-unit structure */
	dev_info_t		*dip;
	kmutex_t		mutex;
	kcondvar_t		cv;
	volatile int		via_state;	/* driver state */
	volatile int		playing;
	ddi_iblock_cookie_t	iblock_cookie;	/* for mutexes */

	caddr_t			pci_regs;
	ddi_acc_handle_t	pci_regs_handle;

	caddr_t			sgd_regs;
	ddi_acc_handle_t	sgd_regs_handle;

	Sdma			sgdt_dma;
	Sdma			buf_dma;

	volatile int		free_index;
	volatile int		n_free;
	volatile int		frag_avail;
} Via;

/*
 * via_state
 */
#define	VIA_OPEN	0x01

#ifndef	TRUE
#define	TRUE	1
#define	FALSE	0
#endif	TRUE

#ifndef	OK
#define	OK	0
#endif	OK

#ifdef	__cplusplus
}
#endif

#endif	/* _VIA_REG_H */
