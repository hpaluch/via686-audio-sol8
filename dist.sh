#!/bin/sh

# make distribution package

REL=0.0.1
gtar cfz ../via-${REL}.tar.gz *.c *.h Makefile* i386/via \
    ./inst ./runmad ./dist README
